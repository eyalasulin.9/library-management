#pragma once
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include <fstream>
#include <iomanip>
#include <sstream>

class ManageMsg
{
	static char* getPartFromSocket(SOCKET sc, int bytesNum);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
public:
	static int getMessageTypeCode(SOCKET sock);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getPaddedNumber(int num, int digits);
	static void sendMsg(SOCKET sock, std::string msg);
};