#include "AccessDB.h"

//GLOBAL variable for callback function
std::string value;

//callback funcitons
int takenStateCallback(void* data, int argc, char** argv, char** azColName)
{
	value = argv[0];
	return 0;
}

//I know it looks awful...
int searchBookCallback(void* data, int argc, char** argv, char** azColName)
{
	std::string zero = "0";
	value += argv[0];
	value += "\t";
	value += argv[1];
	value += "\t";
	value += argv[2];
	value += "\t";
	value += !zero.compare(argv[3]) ? "Free" : "Taked";
	std::cout << argv[3];
	value += "\n";
	return 0;
}

//Constructor
AccessDB::AccessDB()
{
	this->_errmsg = nullptr;
	int res = sqlite3_open("library.db", &(this->_db));
	if (res) 
	{
		this->_db = nullptr;
		throw ("Can't open database: %s\n", sqlite3_errmsg(this->_db));
	}
	else
	{
		std::cout << "database opened successfully" << std::endl;
	}
}

//Distructor
AccessDB::~AccessDB()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

//Take a book - update database table
bool AccessDB::take_book(int book_id, int customer_id)
{
	std::string query = "SELECT TAKEN FROM BOOKS WHERE ID == " + std::to_string(book_id) + ";";
	this->_errmsg = nullptr;
	int res = sqlite3_exec(this->_db, query.c_str(), takenStateCallback, nullptr, this->_errmsg);
	if (value == "0" && res == SQLITE_OK)
	{
		query = "UPDATE BOOKS set TAKEN = " + std::to_string(customer_id) + " WHERE ID == " + std::to_string(book_id) + ";";
		this->_errmsg = nullptr;
		res = sqlite3_exec(this->_db, query.c_str(), nullptr, nullptr, this->_errmsg);
	}
	else
	{
		value = "";
		return false;
	}
	value = "";
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}

//Return a book that taked - update database table
bool AccessDB::return_book(int book_id)
{
	std::string query = "SELECT TAKEN FROM BOOKS WHERE ID == " + std::to_string(book_id) + ";";
	this->_errmsg = nullptr;
	int res = sqlite3_exec(this->_db, query.c_str(), takenStateCallback, nullptr, this->_errmsg);
	if (value != "0" && res == SQLITE_OK)
	{
		std::string query = "UPDATE BOOKS set TAKEN = 0 WHERE ID == " + std::to_string(book_id) + ";";
		this->_errmsg = nullptr;
		int res = sqlite3_exec(this->_db, query.c_str(), nullptr, nullptr, this->_errmsg);
	}
	else
	{
		value = "";
		return false;
	}
	value = "";
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}

//Search a book in the books table by search key(name) - select from database table
std::string AccessDB::search_book(std::string name)
{
	std::string query = "SELECT * FROM BOOKS WHERE LOWER(NAME) LIKE '%" + name + "%';";
	this->_errmsg = nullptr;
	int res = sqlite3_exec(this->_db, query.c_str(), searchBookCallback, nullptr, this->_errmsg);
	if (res != SQLITE_OK || value.empty())
	{
		return "No Results!";
	}
	std::string temp = "ID: NAME:\t\t\t\tAUTHOR:\t\t\tSTATUS:\n";
	temp += value;
	value = "";
	return temp;
}
 
//Add a new customer - update database table
bool AccessDB::add_customer(int id, std::string name, std::string phone)
{
	std::string query = "INSERT INTO CUSTOMERS VALUES (" + std::to_string(id) + ",'" + name + "','" + phone + "');";
	this->_errmsg = nullptr;
	int res = sqlite3_exec(this->_db, query.c_str(), nullptr, nullptr, this->_errmsg);
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}
