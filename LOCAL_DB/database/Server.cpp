#include "Server.h"

std::vector<std::thread> Server::_threads;

//Constructor
Server::Server()
{
	this->_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_sock == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

//Distructor
Server::~Server()
{
	for (auto& t : this->_threads)
	{
		t.detach();
	}
	try
	{
		closesocket(this->_sock);
	}
	catch (...) {};
}

//This funciton listens for new clients and creates them
void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	//binding
	if (bind(this->_sock, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	std::cout << "binded" << std::endl;

	//listening
	if (listen(this->_sock, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	std::cout << "listening..." << std::endl;

	while (true)
	{
		std::cout << "accepting client..." << std::endl;
		accept();
	}
}

//Accept socket connection
void Server::accept()
{
	SOCKET& client_socket = *(new SOCKET(_WINSOCKAPI_::accept(this->_sock, NULL, NULL)));

	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}

	std::cout << "Client accepted!" << std::endl;

	//create client's handler thread 
	this->_threads.emplace_back([&]() {clientHandler(client_socket); });
}

//This function is the client handler function
void Server::clientHandler(SOCKET clientSocket)
{
	//initilaize mutex locker
	std::unique_lock<std::mutex> locker(this->_dbMutex);
	locker.unlock();

	int msgCode = 0, len = 0, bookId = 0, customerId = 0;
	std::string bookName, phoneNumber, customerName, value;
	bool state = false;

	try
	{
		while (true)
		{
			msgCode = ManageMsg::getMessageTypeCode(clientSocket);
			switch (msgCode)
			{

			case typeCodes::TAKE:
				bookId = ManageMsg::getIntPartFromSocket(clientSocket, 4);
				customerId = ManageMsg::getIntPartFromSocket(clientSocket, 9);
				std::cout << bookId << "   " << customerId << std::endl;
				
				locker.lock();
				state = this->_db.take_book(bookId, customerId);
				locker.unlock();
				if (state)
				{
					ManageMsg::sendMsg(clientSocket, "222");
				}
				else
				{
					ManageMsg::sendMsg(clientSocket, "333");
				}
				break;

			case typeCodes::RETURN:
				bookId = ManageMsg::getIntPartFromSocket(clientSocket, 4);
				std::cout << bookId << std::endl;
				
				locker.lock();
				state = this->_db.return_book(bookId);
				locker.unlock();
				if (state)
				{
					ManageMsg::sendMsg(clientSocket, "222");
				}
				else
				{
					ManageMsg::sendMsg(clientSocket, "333");
				}
				break;

			case typeCodes::SEARCH_BOOK:
				len = ManageMsg::getIntPartFromSocket(clientSocket, 2);
				bookName = ManageMsg::getStringPartFromSocket(clientSocket, len);
				std::cout << bookName << std::endl;

				locker.lock();
				value.assign(this->_db.search_book(bookName));
				locker.unlock();
				ManageMsg::sendMsg(clientSocket, "203");
				ManageMsg::sendMsg(clientSocket, value);
				break;

			case typeCodes::ADD_CUSTOMER:
				customerId = ManageMsg::getIntPartFromSocket(clientSocket, 9);
				len = ManageMsg::getIntPartFromSocket(clientSocket, 2);
				customerName = ManageMsg::getStringPartFromSocket(clientSocket, len);
				phoneNumber = ManageMsg::getStringPartFromSocket(clientSocket, 10);
				std::cout << customerId << "\t" << customerName << "\t" << phoneNumber << std::endl;

				locker.lock();
				state = this->_db.add_customer(customerId, customerName, phoneNumber);
				locker.unlock();
				if (state)
				{
					ManageMsg::sendMsg(clientSocket, "222");
				}
				else
				{
					ManageMsg::sendMsg(clientSocket, "333");
				}
				break;
			}

		}
	}
	catch (...) //disconnection discovered by overflow of data that sent from the client
	{
		std::cout << "client disconnected" << std::endl;
	}
}