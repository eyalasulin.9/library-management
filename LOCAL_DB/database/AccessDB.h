#include "sqlite3.h"
#include <iostream>
#include <string>

int takenStateCallback(void* data, int argc, char** argv, char** azColName);
int searchBookCallback(void* data, int argc, char** argv, char** azColName);

class AccessDB
{
	sqlite3* _db;
	char** _errmsg;

public:
	AccessDB();
	~AccessDB();
	bool take_book(int book_id, int customer_id);
	bool return_book(int book_id);
	std::string search_book(std::string name);
	bool add_customer(int id, std::string name, std::string phone);
};