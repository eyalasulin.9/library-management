#pragma comment (lib, "ws2_32.lib")
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>

#include "WSAInitializer.h"
#include "Server.h"

/*
Multi-Client Database Server with SQLITE3
Using:
-threads
-mutexes
-sockets
-Exceptinos
-OOP Programming

I emphasized the modularity of the project and the files division
*/
int main(void)
{
	std::cout << "starting..." << std::endl;
	try
	{
		WSAInitializer wsaInit;
		Server server;
		server.serve(8388);
	}
	catch (std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}

	system("pause");
	return 0;
}