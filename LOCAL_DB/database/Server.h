#pragma once
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <thread>
#include <mutex>

#include "ManageMsg.h"
#include "AccessDB.h"

enum typeCodes : byte
{
	 TAKE = 101,
	 RETURN,
	 SEARCH_BOOK,
	 ADD_CUSTOMER
};

class Server
{
	SOCKET _sock;
	AccessDB _db;
	std::mutex _dbMutex;

	static std::vector<std::thread> _threads;
	void accept();
	void clientHandler(SOCKET clientSocket);

public:
	Server();
	~Server();
	void serve(int port);
};