from DBconnection import DBconnection
from userInterface import *
import sys

"""
Client for database server
Using Sockets, Exceptions and OOP programming

I emphasized the modularity of the project and the files division
"""


def main():
    functions = {'1': "db.take_book(params_take_book())", '2': "db.return_book(params_return_book())", '3': "db.search_book(params_search_book())", '4': "db.add_customer(params_add_customer())"}
    try:
        db = DBconnection(8388, '127.0.0.1')  # try to connect to the local database
    except Exception as error:
        print("Something went wrong ->", error)
        sys.exit(1)

    while True:
        menu()
        selection = select()
        if selection == "5":
            sys.exit(0)
        exec(functions.get(selection))  # Run the appropriate function by calling from the dict
        print(db.response())


if __name__ == '__main__':
    main()