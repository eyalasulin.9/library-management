import socket


class DBconnection:
    def __init__(self, port, host):
        self.sock = socket.socket()
        self.sock.connect((host, port))

    # database request for taking a book
    def take_book(self, params):
        book_id, customer_id = params
        msg = "101" + str(book_id).zfill(4) + str(customer_id)
        self.sock.sendall(msg.encode())

    # database request for returning a book
    def return_book(self, book_id):
        msg = "102" + str(book_id).zfill(4)
        self.sock.sendall(msg.encode())

    # database request for searching a book
    def search_book(self, name):
        msg = "103" + str(len(name)).zfill(2) + name
        self.sock.sendall(msg.encode())

    # database request for add a new customer
    def add_customer(self, params):
        id, name, phone = params
        msg = "104" + str(id) + str(len(name)).zfill(2) + name + phone
        self.sock.sendall(msg.encode())

    # receive data from the database
    def response(self):
        code = self.sock.recv(3).decode()
        if code == '222':
            return "Done!"
        if code == '333':
            return "Failed!"
        if code == '203':
            data = self.sock.recv(1024).decode()
            return data

    def __del__(self):
        self.sock.close()
