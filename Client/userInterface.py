# Print options menu
def menu():
    print("Select one of the actions below:\n"
          "1. Take a book\n"
          "2. Return a book\n"
          "3. Search a book id\n"
          "4. Add new customer\n"
          "5. Exit")


# Input from user
def select():
    num = 0
    count = 0
    while True:
        num = input()
        if num.isdigit() and 5 >= int(num) >= 1: # Check validity
            break
        print("Please enter a number between 1 to 4")
    return num


# Input from user- Take a book func parameters
def params_take_book():
    book_id = input("Enter book id: ")
    customer_id = input("Enter customer id: ")
    return book_id, customer_id


# Input from user- Return a book func parameters
def params_return_book():
    book_id = input("Enter book id: ")
    return book_id


# Input from user- Search a book func parameters
def params_search_book():
    book_name = input("Enter book name: ")
    return book_name.lower()  # Synced with the database program


# Input from user- Add a customer func parameters
def params_add_customer():
    customer_id = input("Enter customer id: ")
    customer_name = input("Enter customer name: ")
    customer_phone = input("Enter customer phone: ")
    return customer_id, customer_name, customer_phone

